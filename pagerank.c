#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void read_file();
void calculate_pagerank();
void write_to_file();
double damping_factor = 0.85;
double epsilon = 1e-6;
char *output_file = "output";
char *input_file = "stanford-08-03.graph";
int max_num_of_nodes;
int iter_count = 1;
unsigned short cur_arr = 1;
double *pagerank[2];
typedef struct node{
	int node_id;
	int num_of_adj_nodes;
	int *adj_nodes;
	struct node *next;
} Node;
Node *head_ptr = NULL;
int main(int argc, char *argv[]){
	int i,j;
	for(i=1;i<argc;++i){
		if( strcmp(argv[i],"-d")==0 ){
			damping_factor = atof(argv[++i]);
		}
		else if( strcmp(argv[i],"-e")==0 ){
			epsilon = atof(argv[++i]);
		}
		else if( strcmp(argv[i],"-o")==0 ){
			output_file = argv[++i];
		}
		else input_file = argv[i];
	}
	printf("damping = %lf\nepsilon = %.10lf\noutput = %s\ninput = %s\n", damping_factor, epsilon, output_file, input_file);
	read_file();
	for(i=0;i<2;++i){
		pagerank[i] = (double *)malloc(sizeof(double)*max_num_of_nodes);
		for(j=0;j<max_num_of_nodes;++j){
			if(i==0) pagerank[i][j] = 1;
			else pagerank[i][j] = 0;
		}
	}
	calculate_pagerank();
	write_to_file();
	return 0;
}
void read_file(){
	printf("Reading the file: %s\n", input_file);
	FILE *fp = fopen(input_file,"r");
	if( fp == NULL ){
		perror("Error while opening the file.\n");
		exit(EXIT_FAILURE);
	}
	int i;
	fscanf(fp,"#maxnode %d",&max_num_of_nodes);
	Node *current_ptr = NULL;
	Node *previous_ptr = NULL;
	while(1){ // load the nodes in the file into memory
		current_ptr = (Node *)malloc(sizeof(Node)); // create a node
		current_ptr->next = NULL;
		if( !head_ptr ) head_ptr = current_ptr; // set head ptr
		fscanf(fp,"%d:%d",&current_ptr->node_id, &current_ptr->num_of_adj_nodes);
		if( feof(fp) ) break; // end of file
		current_ptr->adj_nodes = (int *)malloc(sizeof(int)*current_ptr->num_of_adj_nodes); // list of adjacent nodes
		for(i=0;i < current_ptr->num_of_adj_nodes; ++i){
			fscanf(fp,"%d", &current_ptr->adj_nodes[i]);
		}
		if( previous_ptr) previous_ptr->next = current_ptr;
		previous_ptr = current_ptr;
	}
	fclose(fp);
}
void calculate_pagerank(){
	Node *current_ptr = head_ptr;
	int i, next_node = 1;
	double accumulate_pagerank_without_outlink = 0;
	while(current_ptr){
		if( next_node != current_ptr->node_id ){ // handle the node without outlinks
			for(;next_node < current_ptr->node_id; ++next_node){
				accumulate_pagerank_without_outlink += pagerank[1-cur_arr][next_node-1]/(max_num_of_nodes-1);
				pagerank[cur_arr][next_node-1] -= pagerank[1-cur_arr][next_node-1]/(max_num_of_nodes-1);
			}
		}
		for(i=0;i < current_ptr->num_of_adj_nodes; ++i){
			pagerank[cur_arr][current_ptr->adj_nodes[i]-1] += pagerank[1-cur_arr][current_ptr->node_id-1]/current_ptr->num_of_adj_nodes;
		}
		++next_node;
		current_ptr = current_ptr->next;
	}
	for(;next_node <= max_num_of_nodes; ++next_node){ // calculate the node without outlinks at tail
		accumulate_pagerank_without_outlink += pagerank[1-cur_arr][next_node-1]/(max_num_of_nodes-1);
		pagerank[cur_arr][next_node-1] -= pagerank[1-cur_arr][next_node-1]/(max_num_of_nodes-1);
	}
	double sum = 0;
	for(i=0;i<max_num_of_nodes;++i){ // update the pagerank of this iteration
		pagerank[cur_arr][i] = 1 - damping_factor + damping_factor * (pagerank[cur_arr][i] + accumulate_pagerank_without_outlink);
		sum += pow(pagerank[cur_arr][i] - pagerank[1-cur_arr][i], 2); // calculate the norm2 distance
		pagerank[1-cur_arr][i] = 0; // initialize to 0
	}
	printf("Iter %d, Norm = %.10lf\n", iter_count++, sqrt(sum));
	if(sqrt(sum)<epsilon){ // terminate recursive call
		printf("DONE!!!\n");
		return;
	}
	cur_arr = 1 - cur_arr; // flip
	calculate_pagerank(); // calculate pagerank recursively
}
void write_to_file(){
	FILE *fp = fopen(output_file, "w");
	if( fp == NULL ){
		perror("Error while writing the file.\n");
		exit(EXIT_FAILURE);
	}
	int i;
	for(i=0;i<max_num_of_nodes;++i){
		fprintf(fp,"%d:%lf\n",i+1,pagerank[cur_arr][i]);
		//printf("%d:%lf\n",i+1,pagerank[cur_arr][i]);
	}
	fclose(fp);
}
